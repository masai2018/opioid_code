library(methods)
source("e:/CT_APCD/Sai/data-organization/R/utils.R")
pkgs <- c("data.table", "lubridate", "readr", "tidyverse",
          NULL)
need.packages(pkgs)

acc_dir <- input_dir('E:/KHIN/2-6-2019/Final Delivery 2019-01-18/raw data/patient_account/')
acc_files <- list.files(acc_dir)

con_dir <- input_dir('E:/KHIN/2-6-2019/Final Delivery 2019-01-18/raw data/patient_contact/')
con_files <- list.files(con_dir)

med_dir <- input_dir("E:/KHIN/2-6-2019/Final Delivery 2019-01-18/raw data/medication/")
med_files <- list.files(med_dir, pattern = ".csv")

dgx_dir <- input_dir("E:/KHIN/2-6-2019/Final Delivery 2019-01-18/raw data/patient_diagnosis/")
dgx_files <- list.files(dgx_dir, pattern = ".csv")

ks_zip <- fread("E:/KHIN/Sai/other_files/ks_zip.csv",
                header = TRUE,
                encoding = "UTF-8",
                colClasses = "character")

## contact files
test <- fread(paste0(acc_dir, acc_files[1]),
              header = TRUE,
              encoding = "UTF-8",
              colClasses = "character")
names(test)
dat_state <- rbindlist(lapply(con_files, function(file)
{
  tmp <- fread(paste0(con_dir, file), 
               header = TRUE,
               encoding = "UTF-8",
               colClasses = "character",
               select = c("MPI", "state", "zip"))
  tmp <- tmp[MPI != ""]
  tmp[zip != "", zip := substr(zip, start = 1, stop = 5)]
  tmp[zip %in% ks_zip$`ZIP Code` & state == "", 
      state := "KS"]
  tmp <- tmp[!duplicated(tmp, by = c("MPI", "state"))]
  dup_mpi <- tmp[, .N,
                 by = .(MPI)][N > 2]$MPI
  tmp1 <- tmp[MPI %in% dup_mpi][state != "" &
                                  zip != ""]
  tmp2 <- tmp[!MPI %in% dup_mpi]
  tmp <- rbind(tmp1, tmp2)
  tmp <- tmp[!duplicated(tmp)]
  return(tmp)
}))

dat_state <- dat_state[!duplicated(dat_state, by = c("MPI", "state"))]
dat_state[zip %in% ks_zip$`ZIP Code` & state == "", 
          state := "KS"]
ks_list <- c("kansas", "Kansas", "Kasnas", "Knasas",
             "ks", "kS", "Ks", "KS")
mpi_ks <- dat_state[state %in% ks_list, unique(MPI)]
all_state <- unique(dat_state[, .(state)])
fwrite(all_state, 
       file = "E:/KHIN/Sai/in_process/all_state_names.csv")
fwrite(data.table(MPI = mpi_ks), 
       file = "E:/KHIN/Sai/in_process/ks_mpi.csv")
mpi_other <- dat_state[!MPI %in% ks_list, unique(MPI)]
(ks_rate <- length(mpi_ks)/length(dat_state[, unique(MPI)]))  # 0.8859163

## dgx files
testdgx <- fread(paste0(dgx_dir, dgx_files[2]),
                 header = TRUE,
                 encoding = "UTF-8",
                 colClasses = "character")
names(testdgx)
dgx_dat <- rbindlist(lapply(dgx_files, function(file){
  tmp <- fread(paste0(dgx_dir, file),
               colClasses = "character",
               header = TRUE,
               encoding = "UTF-8",
               select = c("patient_account_id",
                          "diagnosis_timestamp",
                          "MPI",
                          NULL))
  tmp <- tmp[MPI %in% mpi_ks, list(patient_account_id,
                                   diagnosis_timestamp)]
  tmp <- tmp[!duplicated(tmp)]
  tmp[, years := year(ymd_hms(diagnosis_timestamp))]
  tmp <- tmp[!is.na(years) &
               as.integer(years) > 1990 &
               as.integer(years) < 2019]
  return(tmp)
}))
dgx_dat[, years := year(ymd_hms(diagnosis_timestamp))]
dgx_dat2 <- dgx_dat[, list(MPI, years)]
dgx_dat2 <- dgx_dat2[!duplicated(dgx_dat2)]
dgx_dat_smy <- dgx_dat2[, lapply(.SD, unique), 
                        by = .(years),
                        .SDcols = "MPI"]
dgx_dat_smy2 <- dgx_dat_smy[, .N, by = years][, N := -sort(-N)]
setnames(dgx_dat_smy2, "N", "number_of_MPI")
sum(dgx_dat_smy2[is.na(years)|
               as.integer(years) < 1990 |
               as.integer(years) > 2019]$number_of_MPI)/sum(dgx_dat_smy2$number_of_MPI)
## 60.19552% is meaningless (from 100/1115 dgx files)
fwrite(dgx_dat_smy2,
       file = "E:/KHIN/Sai/output/summary_files/MPI_from_dgx_by_year_KS.csv")


##claim files
acc_dat <- rbindlist(lapply(acc_files, function(file){
  tmp <- fread(paste0(acc_dir, file),
               colClasses = "character",
               header = TRUE,
               encoding = "UTF-8",
               select = c("vault_acc_id",
                          "event_timestamp",
                          "MPI",
                          NULL))
  tmp <- tmp[MPI %in% mpi_ks]
  return(tmp)
}))
acc_dat[, years := year(ymd_hms(event_timestamp))]
acc_dat2 <- acc_dat[, list(MPI, years)]
acc_dat2 <- acc_dat2[!duplicated(acc_dat2)]
acc_dat_smy <- acc_dat2[, lapply(.SD, unique), 
                        by = .(years),
                        .SDcols = "MPI"]
acc_dat_smy2 <- acc_dat_smy[, .N, by = years][, N := -sort(-N)]
setnames(acc_dat_smy2, "N", "number_of_MPI")
fwrite(acc_dat_smy2,
       file = "E:/KHIN/Sai/output/summary_files/MPI_from_claim_by_year_KS.csv")


## discover missing year
length(acc_dat_smy[years >= 2010 & years <= 2018, unique(MPI)]) /
  length(acc_dat_smy[, unique(MPI)])  #0.9991923
length(acc_dat_smy[years >= 2013 & years <= 2018, unique(MPI)]) /
  length(acc_dat_smy[, unique(MPI)])  #0.9938717
sum(acc_dat_smy[years == 1899, unique(MPI)] %in% 
      acc_dat_smy[years == 2017, unique(MPI)])/
  acc_dat_smy2[years == 1899]$N # 0.01997179
length(intersect(acc_dat_smy[is.na(years), unique(MPI)],
      acc_dat_smy[years == 2017, unique(MPI)]))/
  acc_dat_smy2[is.na(years)]$N # 0.0001928599

## deal with medication data 1
acc_dat_tmp <- acc_dat[, list(vault_acc_id, years)]
acc_dat_tmp <- acc_dat_tmp[!duplicated(acc_dat_tmp)]
med_dat <- rbindlist(lapply(med_files, function(file){
  tmp <- fread(paste0(med_dir, file),
               colClasses = "character",
               header = TRUE,
               encoding = "UTF-8",
               select = c("patient_account_id",
                          # "event_timestamp",
                          "MPI",
                          NULL))
  tmp <- tmp[MPI %in% mpi_ks]
  tmp <- tmp[!duplicated(tmp)]
  tmp <- dgx_dat[tmp,
                 on = "patient_account_id"]
  tmp <- tmp[, list(MPI, years)]
  tmp <- tmp[!duplicated(tmp)]
  return(tmp)
}))

# med_dat2 <- med_dat[MPI %in% mpi_ks]
# sum(unique(med_dat$MPI) %in% mpi_ks)/length(unique(med_dat$MPI))
# # 8146869% mpi's in KS state
# med_dat2 <- dgx_dat[med_dat2, 
#                    on = "patient_account_id"]
# med_dat2 <- med_dat2[, list(MPI, years)]
med_dat <- med_dat[!duplicated(med_dat)]
med_dat_smy <- med_dat[, lapply(.SD, unique), 
                        by = .(years),
                        .SDcols = "MPI"]
med_dat_smy2 <- med_dat_smy[, .N, by = years][, N := -sort(-N)]
setnames(med_dat_smy2, "N", "number_of_MPI")
fwrite(med_dat_smy2,
       file = "E:/KHIN/Sai/output/summary_files/MPI_from_medication_year_from_dgx_KS.csv")


## deal with medication data 2
med_dat_2 <- rbindlist(lapply(med_files, function(file){
  tmp <- fread(paste0(med_dir, file),
               colClasses = "character",
               header = TRUE,
               encoding = "UTF-8",
               select = c(
                 ## "patient_account_id",
                          "start_date",
                          "MPI",
                          NULL))
  tmp[, years := year(ymd_hms(start_date))]
  tmp <- tmp[, list(MPI, years)]
  tmp <- tmp[!duplicated(tmp)]
  return(tmp)
}))

med_dat_2 <- med_dat_2[MPI %in% mpi_ks]
med_dat_2 <- med_dat_2[!duplicated(med_dat_2)]
med_dat_smy_2 <- med_dat_2[, lapply(.SD, unique), 
                        by = .(years),
                        .SDcols = "MPI"]
med_dat_smy_2 <- med_dat_smy_2[, .N, by = years][, N := -sort(-N)]
fwrite(med_dat_smy_2,
       file = "E:/KHIN/Sai/output/summary_files/MPI_from_medication_by_year_KS_2.csv")

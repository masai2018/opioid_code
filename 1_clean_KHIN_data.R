library(methods)
library(data.table)
source("e:/CT_APCD/Sai/data-organization/R/utils.R")
indir <- input_dir("E:/KHIN/Sai/in_process/medication_mpi/")
infiles <- list.files(indir, pattern=".csv")
outdir <- output_dir("e:/KHIN/Sai/in_process/modified_data/")
col_kp <- c(
  # "race",
             ## "index_update_dt_tm",
             ## "source",
             ## "source_type",
             "MPI",
             ## "mpi_update",
             # "vault_pid",
             "id",
             ## "mrn",
             ## "prefix",
             ## "firstname",
             ## "middlename", 
             ## "lastname",
             ## "suffix",
             "dob",
             "sex",
             ## "ethnicity",
             ## "citizenship",
             ## "nationality",
             ## "language",
             ## "religion",
             "marital_status",
             ## "mil_status",
             ## "student_status",
             # "hipaa",
             # "deceased",
             ##"mother_maiden_name",
             "event_timestamp",
             # "created",
             # "updated",
             # "collect_registered",
             # "collect_cmp",
             ##"stable_document_id",
             ##"ssn",                         
             ## "dlnum",
             ##"dlstate",                     
             ## "alt_id_name",
             ## "alt_id_num",                  
             ## "policy_id",
             ## "discharge_status_code",
             ## "discharge_status_description",
             # "accid_ref",
             "modified_race",
             ## "i.id",
             ## "i.index_update_dt_tm",
             ## "i.source",
             ## "i.source_type",
             ## "vault_based_id",
             ## "patient_id",
             # "patient_account_id",
             "start_date",
            "stop_date",
             "code",
             "code_system",
             "name",
             "provider_id",
             ## "route",
             ## "sig",
             ## "strength",
             ## "packaging",
             "dosage",
             NULL)

dt <- data.table()
for(fl in infiles){
  tmp <- fread(paste0(indir, fl),
               colClasses = "character",
               header = TRUE,
               encoding = "UTF-8",
               select = col_kp)
  tmp <- tmp[!duplicated(tmp)]
  dt <- rbind(dt, tmp)
  rm(tmp)
  dt <- dt[!duplicated(dt)]
}
outdir2 <- output_dir("E:/KHIN/Sai/in_process/modified_data_unique/")
fwrite(dt, file = paste0(outdir2, "medication_all_unique.csv"))

## add opioid flag
rx_op <- fread("E:/KHIN/Sai/other_files/Opioid_RxNorm.csv",
                   colClasses = "character",
                   header = TRUE,
                   encoding = "UTF-8")[, unique(RXCUI)]
outdir2 <- output_dir("E:/KHIN/Sai/in_process/modified_data_unique/")
i <- 1
t_n <- length(infiles)
for(fl in infiles){
  tmp <- fread(paste0(indir, fl),
               colClasses = "character",
               header = TRUE,
               encoding = "UTF-8",
               select = col_kp)[, op_flag := "N"]
  # tmp <- tmp[!duplicated(tmp)]
  tmp[code %in% rx_op, op_flag := "Y"]
  fwrite(tmp, 
         file = paste0(outdir2, "medication_all_for_opioid_freq.csv"),
         append = TRUE)
  # dt2 <- rbind(dt2, tmp)
  rm(tmp)
  cat(paste0("The ", i,
                "/", t_n,
                " has been done!",
                "\n"))
  i <- i + 1
}
